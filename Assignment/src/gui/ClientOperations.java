package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dotNet.serv.ArrayOfPackage;
import dotNet.serv.ArrayOfRouteEntry;
import dotNet.serv.ClientServiceDotNet;
import dotNet.serv.ClientServiceDotNetSoap;
import dotNet.serv.Package;
import dotNet.serv.RouteEntry;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextPane;

public class ClientOperations extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private ClientServiceDotNet clientServiceDotNet;
	private ClientServiceDotNetSoap clientServiceDotNetSoap;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientOperations frame = new ClientOperations();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClientOperations() {
		clientServiceDotNet=new ClientServiceDotNet();
		clientServiceDotNetSoap=clientServiceDotNet.getPort(ClientServiceDotNetSoap.class);
		
		setTitle("Client Operatios");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 356);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnLogout = new JMenu("Logout");
		menuBar.add(mnLogout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, -30, 594, 275);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("List packages", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblSearchPackages = new JLabel("Search packages");
		lblSearchPackages.setBounds(10, 11, 108, 14);
		panel.add(lblSearchPackages);
		
		textField_1 = new JTextField();
		textField_1.setBounds(45, 36, 160, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblName = new JLabel("name");
		lblName.setBounds(10, 36, 46, 14);
		panel.add(lblName);
		
		JLabel lblDescription = new JLabel("description");
		lblDescription.setBounds(215, 39, 95, 14);
		panel.add(lblDescription);
		
		textField_2 = new JTextField();
		textField_2.setBounds(273, 36, 180, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		

		
		JLabel lblChoosePackage = new JLabel("Choose package");
		lblChoosePackage.setBounds(10, 77, 108, 14);
		panel.add(lblChoosePackage);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 127, 300, 109);
		panel.add(textArea);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(112, 74, 198, 20);
		ArrayOfPackage arrayPackages=clientServiceDotNetSoap.getAllMyPackages(Login.clientId);
		List<Package>packages=arrayPackages.getPackage();
		for(int i=0;i<packages.size();i++)
			comboBox.addItem(packages.get(i).getId());
		
		comboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if(comboBox.getSelectedItem()!=null)
                {
                	for(int i=0;i<packages.size();i++)
                		if(packages.get(i).getId()==Integer.valueOf(comboBox.getSelectedItem().toString()))
                		{
                			textArea.setText("Name= "+packages.get(i).getName()+"\n"
                							+ "Description= "+packages.get(i).getDescription()+"\n"
                						    + "Sender City= "+packages.get(i).getSenderCity().getName()+"\n"
                						    +"Receiver name= "+packages.get(i).getReceiver().getName()+"\n"
                						    + "Receiver city= "+packages.get(i).getReceiverCity().getName());
                		}
                	
                }
            }
        });
		panel.add(comboBox);
		
		JButton btnSearch_1 = new JButton("SEARCH");
		btnSearch_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=textField_1.getText();
				String description=textField_2.getText();
				ArrayOfPackage arrayPackages=clientServiceDotNetSoap.searchPackagesByNameAndDescription(Login.clientId, name, description);
				comboBox.removeAllItems();
				List<Package>packages=arrayPackages.getPackage();
				for(int i=0;i<packages.size();i++)
					comboBox.addItem(packages.get(i).getId());
			}
		});
		btnSearch_1.setBounds(490, 35, 89, 23);
		panel.add(btnSearch_1);
		
		JLabel lblInformationsAboutSelected = new JLabel("Informations about selected package");
		lblInformationsAboutSelected.setBounds(10, 102, 300, 14);
		panel.add(lblInformationsAboutSelected);
		
		JTextArea textArea_2 = new JTextArea();
		textArea_2.setBounds(330, 127, 249, 109);
		panel.add(textArea_2);
		
		JButton btnCheckPackageStatus = new JButton("CHECK PACKAGE STATUS");
		btnCheckPackageStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int packageId=Integer.valueOf(comboBox.getSelectedItem().toString());
				ArrayOfRouteEntry arrayOfRouteEntry=clientServiceDotNetSoap.checkPackageStatus(packageId);
				List<RouteEntry> routeEntry=arrayOfRouteEntry.getRouteEntry();
				for(int i=routeEntry.size()-1;i>=0;i--)
				{
					textArea_2.append("\n"+"-------------------"+"\n");
					textArea_2.append("City="+routeEntry.get(i).getCity().getName()+"\n");
					textArea_2.append("Postal code="+routeEntry.get(i).getCity().getName()+"\n");
					textArea_2.append("Timestamp="+routeEntry.get(i).getTimestamp()+"\n");
				}
			}
			
			
		});
		btnCheckPackageStatus.setBounds(330, 73, 249, 23);
		panel.add(btnCheckPackageStatus);
		
		

	}
}
