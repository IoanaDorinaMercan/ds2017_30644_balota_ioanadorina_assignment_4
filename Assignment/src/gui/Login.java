package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import dotNet.serv.ClientServiceDotNet;
import dotNet.serv.ClientServiceDotNetSoap;
import entities.Client;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.awt.event.ActionEvent;

import services.AdminOperations;
import services.AdminOperationsImpl;
import javax.swing.JPasswordField;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private static Login f;
	private ClientServiceDotNet clientServiceDotNet;
	private ClientServiceDotNetSoap clientServiceDotNetSoap;
	public static int clientId;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
					f = frame;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws MalformedURLException
	 */
	public Login() throws MalformedURLException {
		
		clientServiceDotNet=new ClientServiceDotNet();
		clientServiceDotNetSoap=clientServiceDotNet.getPort(ClientServiceDotNetSoap.class);
		
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsername = new JLabel("Email");
		lblUsername.setBounds(10, 59, 83, 14);
		contentPane.add(lblUsername);

		textField = new JTextField();
		textField.setBounds(73, 56, 147, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 96, 65, 14);
		contentPane.add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setBounds(73, 93, 147, 20);
		contentPane.add(passwordField);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String email = textField.getText();
				String password = passwordField.getText();
				dotNet.serv.Client client= clientServiceDotNetSoap.login(email, password);
				clientId=client.getId();
				if (client != null) {

					JOptionPane.showMessageDialog(null, "Success Login");
					System.out.println(client.isIsAdmin());
					try {
						if (client.isIsAdmin() == true) {
							AdminOperationsGUI adminGui = new AdminOperationsGUI();
							f.setVisible(false);
							adminGui.setVisible(true);

						} else {
							ClientOperations clientGui = new ClientOperations();
							f.setVisible(false);
							clientGui.setVisible(true);
						}
					} catch (Exception e) {
					}
				} else {
					JOptionPane.showMessageDialog(null, "Error", "Error Login", JOptionPane.ERROR_MESSAGE);

				}
			}
		});
		btnLogin.setBounds(44, 176, 89, 23);
		contentPane.add(btnLogin);

		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(309, 126, 132, 20);
		contentPane.add(passwordField_1);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = textField_2.getText();
				String email = textField_3.getText();
				String password = passwordField_1.getText();
				if (clientServiceDotNetSoap.register(name, email, password) > 0) {
					JOptionPane.showMessageDialog(null, "New client was addes", "Success Login",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Error", "Error Register", JOptionPane.ERROR_MESSAGE);
				}
				
				/*if (adminOperations.findClient(email) != null) {
					JOptionPane.showMessageDialog(null, "Already exists an client with this email", "Error Register",
							JOptionPane.ERROR_MESSAGE);
				} else {
					if (adminOperations.addClient(name, email, password, false) > 0) {
						JOptionPane.showMessageDialog(null, "New client was addes", "Success Login",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Error", "Error Register", JOptionPane.ERROR_MESSAGE);
					}
				}*/
			}
		});
		btnRegister.setBounds(338, 176, 89, 23);
		contentPane.add(btnRegister);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(230, 59, 46, 14);
		contentPane.add(lblName);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(230, 96, 46, 14);
		contentPane.add(lblEmail);

		JLabel lblPassword_1 = new JLabel("Password");
		lblPassword_1.setBounds(230, 129, 83, 14);
		contentPane.add(lblPassword_1);

		textField_2 = new JTextField();
		textField_2.setBounds(309, 56, 132, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(309, 93, 132, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		

		

	}
}
