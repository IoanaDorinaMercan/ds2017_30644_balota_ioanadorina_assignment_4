package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import entities.City;
import entities.Client;
import entities.Package;
import services.AdminOperations;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminOperationsGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTabbedPane tabbedPane;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_3;
	private AdminOperations adminOperations;
	private JTextField textField_12;
	private JTextField textField_13;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminOperationsGUI frame = new AdminOperationsGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws MalformedURLException
	 */
	public AdminOperationsGUI() throws MalformedURLException {
		URL url = new URL("http://localhost:8080/Assignment/services/AdminOperationsImpl?wsdl");
		QName qname = new QName("http://services/", "AdminOperationsImplService");
		Service service = Service.create(url, qname);
		adminOperations = service.getPort(AdminOperations.class);

		setTitle("Admin Operations");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 612, 349);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 596, 21);
		contentPane.add(menuBar);

		JMenu mnNewMenu = new JMenu("Add/remove");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Add package");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("presses add");
				tabbedPane.remove(panel);
				tabbedPane.remove(panel_1);
				tabbedPane.add(panel_3);

			}
		});
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem mntmRemovePackage = new JMenuItem("Remove package");
		mntmRemovePackage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("presses remove");
				tabbedPane.remove(panel_3);
				tabbedPane.remove(panel_1);
				tabbedPane.add(panel);
			}
		});
		mnNewMenu.add(mntmRemovePackage);



		JMenu mnPackageStatus = new JMenu("Package status");
		mnPackageStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("presses remove");
				tabbedPane.remove(panel_3);
				tabbedPane.remove(panel);
				tabbedPane.add(panel_1);
			}
		});
		menuBar.add(mnPackageStatus);



		JMenu mnLogout = new JMenu("Logout");
		menuBar.add(mnLogout);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 596, 311);
		contentPane.add(tabbedPane);

		panel_3 = new JPanel();
		tabbedPane.addTab("New tab", null, panel_3, null);
		panel_3.setLayout(null);

		JLabel lblSenderInformations = new JLabel("Sender Informations");
		lblSenderInformations.setBounds(20, 11, 134, 14);
		panel_3.add(lblSenderInformations);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 31, 46, 14);
		panel_3.add(lblName);

		textField = new JTextField();
		textField.setBounds(75, 25, 144, 20);
		panel_3.add(textField);
		textField.setColumns(10);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 66, 46, 14);
		panel_3.add(lblEmail);

		textField_1 = new JTextField();
		textField_1.setBounds(75, 60, 144, 20);
		panel_3.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblCity = new JLabel("City");
		lblCity.setBounds(10, 106, 46, 14);
		panel_3.add(lblCity);

		textField_2 = new JTextField();
		textField_2.setBounds(75, 100, 144, 20);
		panel_3.add(textField_2);
		textField_2.setColumns(10);

		JLabel lblPostalCode = new JLabel("Postal code");
		lblPostalCode.setBounds(10, 145, 72, 14);
		panel_3.add(lblPostalCode);

		textField_3 = new JTextField();
		textField_3.setBounds(75, 142, 144, 20);
		panel_3.add(textField_3);
		textField_3.setColumns(10);

		JLabel lblReceiverInformations = new JLabel("Receiver Informations");
		lblReceiverInformations.setBounds(329, 11, 165, 14);
		panel_3.add(lblReceiverInformations);

		JLabel lblName_1 = new JLabel("Name");
		lblName_1.setBounds(288, 31, 46, 14);
		panel_3.add(lblName_1);

		textField_4 = new JTextField();
		textField_4.setBounds(357, 28, 155, 20);
		panel_3.add(textField_4);
		textField_4.setColumns(10);

		textField_5 = new JTextField();
		textField_5.setBounds(355, 63, 157, 20);
		panel_3.add(textField_5);
		textField_5.setColumns(10);

		JLabel lblEmail_1 = new JLabel("Email");
		lblEmail_1.setBounds(288, 66, 46, 14);
		panel_3.add(lblEmail_1);

		JLabel lblCity_1 = new JLabel("City");
		lblCity_1.setBounds(288, 106, 46, 14);
		panel_3.add(lblCity_1);

		textField_6 = new JTextField();
		textField_6.setBounds(357, 103, 155, 20);
		panel_3.add(textField_6);
		textField_6.setColumns(10);

		JLabel lblPostalCode_1 = new JLabel("Postal code");
		lblPostalCode_1.setBounds(288, 145, 72, 14);
		panel_3.add(lblPostalCode_1);

		textField_7 = new JTextField();
		textField_7.setBounds(357, 142, 155, 20);
		panel_3.add(textField_7);
		textField_7.setColumns(10);

		JLabel lblInformationsAboutPackage = new JLabel("Informations about package");
		lblInformationsAboutPackage.setBounds(195, 191, 155, 14);
		panel_3.add(lblInformationsAboutPackage);

		JLabel lblName_2 = new JLabel("Name");
		lblName_2.setBounds(160, 216, 46, 14);
		panel_3.add(lblName_2);

		textField_8 = new JTextField();
		textField_8.setBounds(237, 216, 171, 20);
		panel_3.add(textField_8);
		textField_8.setColumns(10);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(160, 250, 72, 14);
		panel_3.add(lblDescription);

		textField_9 = new JTextField();
		textField_9.setBounds(236, 247, 172, 20);
		panel_3.add(textField_9);
		textField_9.setColumns(10);

		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String senderName = textField.getText();
				String senderEmail = textField_1.getText();
				String senderCityName = textField_2.getText();
				String senderCityPostalCode = textField_3.getText();

				String receiverName = textField_4.getText();
				String receiverEmail = textField_5.getText();
				String receiverCityName = textField_6.getText();
				String receiverCityPostalCode = textField_7.getText();

				String packageName = textField_8.getText();
				String packageDescription = textField.getText();

				Client sender = new Client();
				sender.setName(senderName);
				sender.setEmail(senderEmail);

				Client receiver = new Client();
				receiver.setName(receiverName);
				receiver.setEmail(receiverEmail);

				City senderCity = new City();
				senderCity.setName(senderCityName);
				senderCity.setPostalCode(senderCityPostalCode);

				City receiverCity = new City();
				receiverCity.setName(receiverCityName);
				receiverCity.setPostalCode(receiverCityPostalCode);

				Package pack = new Package();
				pack.setName(packageName);
				pack.setDescription(packageDescription);
				pack.setSender(sender);
				pack.setReceiver(receiver);
				pack.setSenderCity(senderCity);
				pack.setReceiverCity(receiverCity);
				pack.setIsTracking(false);

				if (adminOperations.addPackage(pack) > 0) {
					JOptionPane.showMessageDialog(null, "New package was added", "Success Add Package",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Error adding package", "Error", JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		btnAdd.setBounds(423, 227, 89, 23);
		panel_3.add(btnAdd);
		
		panel_1 = new JPanel();
		panel_1.setToolTipText("Add to tracking");
		panel_1.setLayout(null);
		tabbedPane.addTab("New tab", null, panel_1, null);
		
		JLabel label = new JLabel("Choose package");
		label.setBounds(22, 36, 131, 14);
		panel_1.add(label);
		
		
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(22, 94, 358, 117);
		panel_1.add(textArea);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if(comboBox.getSelectedItem()!=null)
                {
                	textArea.setText(adminOperations.showInformtionsAboutPackage(Integer.valueOf(comboBox.getSelectedItem().toString())));
                }
            }
        });
		comboBox.setBounds(163, 33, 217, 20);
		panel_1.add(comboBox);
		
		JLabel label_1 = new JLabel("Informations about package");
		label_1.setBounds(22, 69, 232, 14);
		panel_1.add(label_1);
		
		JLabel label_2 = new JLabel("City");
		label_2.setBounds(22, 233, 46, 14);
		panel_1.add(label_2);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(91, 224, 163, 20);
		panel_1.add(textField_12);
		
		JLabel label_3 = new JLabel("Postal code");
		label_3.setBounds(22, 258, 67, 14);
		panel_1.add(label_3);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(91, 255, 162, 20);
		panel_1.add(textField_13);
		
		JButton button_1 = new JButton("ADD TO TRACKING");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cityName=textField_12.getText();
				String cityPostalCode=textField_13.getText();
				City city=new City();
				city.setName(cityName);
				city.setPostalCode(cityPostalCode);
				int id=adminOperations.registerForTracking(Integer.valueOf(comboBox.getSelectedItem().toString()), city);
				if(id>0){
					JOptionPane.showMessageDialog(null, "Package registered for tracking", "Success",
							JOptionPane.INFORMATION_MESSAGE);
					
					comboBox.removeAllItems();
					List<Package>packages=adminOperations.findAllPackagesByTrackedStatus(false);
					if(packages!=null&&!packages.isEmpty())
					{
						for(int i=0;i<packages.size();i++)
							comboBox.addItem(packages.get(i).getId());
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error registering for tracking", "Error",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		button_1.setBounds(283, 233, 163, 23);
		panel_1.add(button_1);
		// tabbedPane.setVisible(false);

		panel = new JPanel();
		tabbedPane.addTab("Update  package status", null, panel, null);
		panel.setLayout(null);

		JLabel lblChoosePackage = new JLabel("Choose package");
		lblChoosePackage.setBounds(22, 36, 131, 14);
		panel.add(lblChoosePackage);


		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(22, 94, 358, 102);
		panel.add(textArea_1);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(163, 33, 217, 20);
		comboBox_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if(comboBox_1.getSelectedItem()!=null)
                {
                	textArea_1.setText(adminOperations.showInformtionsAboutPackage(Integer.valueOf(comboBox_1.getSelectedItem().toString())));
                }
            }
        });
		panel.add(comboBox_1);
		
		JLabel lblInformationsAboutPackage_2 = new JLabel("Informations about package");
		lblInformationsAboutPackage_2.setBounds(22, 69, 145, 14);
		panel.add(lblInformationsAboutPackage_2);

		JLabel lblCity_2 = new JLabel("City");
		lblCity_2.setBounds(22, 233, 46, 14);
		panel.add(lblCity_2);

		textField_10 = new JTextField();
		textField_10.setBounds(92, 227, 163, 20);
		panel.add(textField_10);
		textField_10.setColumns(10);

		JLabel lblNewLabel = new JLabel("Postal code");
		lblNewLabel.setBounds(22, 258, 67, 14);
		panel.add(lblNewLabel);

		textField_11 = new JTextField();
		textField_11.setBounds(92, 255, 162, 20);
		panel.add(textField_11);
		textField_11.setColumns(10);

		JButton btnUpdatePackageStatus = new JButton("ADD NEW CITY TO ROUTE");
		btnUpdatePackageStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int packageId=Integer.valueOf(comboBox_1.getSelectedItem().toString());
				String cityName=textField_10.getText();
				String cityPostalCode=textField_11.getText();
				City city=new City();
				city.setName(cityName);
				city.setPostalCode(cityPostalCode);
				if(adminOperations.addNewRoute(packageId, city)>0)
				{
					JOptionPane.showMessageDialog(null, "New city was added to route", "Success",
							JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error adding new city to route", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnUpdatePackageStatus.setBounds(284, 229, 188, 23);
		panel.add(btnUpdatePackageStatus);
		
		JButton btnDeletePackage = new JButton("DELETE PACKAGE");
		btnDeletePackage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int packageId=Integer.valueOf(comboBox_1.getSelectedItem().toString());
				if(adminOperations.deletePackage(packageId)>0)
				{
					comboBox_1.removeAllItems();
					List<Package>packages=adminOperations.findAllPackagesByTrackedStatus(true);
					if(packages!=null&&!packages.isEmpty())
					{
						for(int i=0;i<packages.size();i++)
							comboBox_1.addItem(packages.get(i).getId());
					}
					textArea_1.setText("");
					JOptionPane.showMessageDialog(null, "Package was deleted", "Success",
							JOptionPane.INFORMATION_MESSAGE);
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Errordeleting package", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
			
			
		});
		btnDeletePackage.setBounds(284, 254, 152, 23);
		panel.add(btnDeletePackage);

		
		
		JMenu mnTracking = new JMenu("Tracking");
		menuBar.add(mnTracking);

		JMenuItem mntmAdd = new JMenuItem("Register package for tracking");
		mntmAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("presses remove");
				tabbedPane.remove(panel_3);
				tabbedPane.remove(panel);
				
				List<Package>packages=adminOperations.findAllPackagesByTrackedStatus(false);
				if(packages!=null&&!packages.isEmpty())
				{
					for(int i=0;i<packages.size();i++)
						comboBox.addItem(packages.get(i).getId());
				}
				tabbedPane.add(panel_1);
			}
		});
		mnTracking.add(mntmAdd);
		
		JMenuItem mntmUpdatePackageStatus = new JMenuItem("Update package status");
		mntmUpdatePackageStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("presses remove");
				tabbedPane.remove(panel_3);
				tabbedPane.remove(panel_1);
				
				List<Package>packages=adminOperations.findAllPackagesByTrackedStatus(true);
				if(packages!=null&&!packages.isEmpty())
				{
					for(int i=0;i<packages.size();i++)
						comboBox_1.addItem(packages.get(i).getId());
				}
				tabbedPane.add(panel);
			}
		});
		mnPackageStatus.add(mntmUpdatePackageStatus);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
