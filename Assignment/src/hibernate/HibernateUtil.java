package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static SessionFactory factory; 
	
	public static Session openSession()
	{
		Session openSession=null;
		try {
			 factory = new Configuration().configure("hibernate/hibernate.cfg.xml").buildSessionFactory();
			 openSession=factory.openSession();
		}
		catch (Throwable ex) { 
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); }
		
		return openSession;
	}
}
