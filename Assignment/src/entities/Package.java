package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "package")
@Proxy(lazy = false)
public class Package   implements java.io.Serializable{

	private int id;
	private Client sender;
	private Client receiver;
	private String name;
	private String description;
	private City senderCity;
	private City receiverCity;
	private boolean tracking;
    private Route route;
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	public Client getSender() {
		return sender;
	}
	public void setSender(Client sender) {
		this.sender = sender;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	public Client getReceiver() {
		return receiver;
	}
	public void setReceiver(Client receiver) {
		this.receiver = receiver;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	public City getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(City senderCity) {
		this.senderCity = senderCity;
	}
	
	@Column(name = "isTracking")
	public boolean getIsTracking() {
		return tracking;
	}
	public void setIsTracking(boolean tracking) {
		this.tracking = tracking;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	public City getReceiverCity() {
		return receiverCity;
	}
	public void setReceiverCity(City receiverCity) {
		this.receiverCity = receiverCity;
	}
	
	@OneToOne
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	
	
	
}
