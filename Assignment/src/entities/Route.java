package entities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import entities.Package;

@Entity
@Table(name = "route")
@Proxy(lazy=false)
public class Route implements java.io.Serializable {


	private int id;
	//private Package pack;
	//private List<RouteEntry> routeEntry= new ArrayList<>();;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	/*@OneToOne
	public Package getPack() {
		return pack;
	}
	public void setPack(Package pack) {
		this.pack = pack;
	}*/
	
	//@OneToMany
	/*public List<RouteEntry> getRouteEntry() {
		return routeEntry;
	}
	public void setRouteEntry(List<RouteEntry> routeEntry) {
		this.routeEntry = routeEntry;
	}*/
	
	
	
}
