package services;

import javax.jws.WebService;
import javax.swing.JOptionPane;

import dao.CityDAO;
import dao.ClientDao;
import dao.PackageDAO;
import dao.RouteDAO;
import dao.RouteEntryDao;
import entities.City;
import entities.Client;
import entities.Package;
import entities.Route;
import entities.RouteEntry;

import java.sql.Timestamp;
import java.util.List;

import javax.jws.WebMethod;
@WebService(endpointInterface = "services.AdminOperations")
public class AdminOperationsImpl implements AdminOperations{


	private ClientDao clientDao;
	private CityDAO cityDao;
	private PackageDAO packageDao;
	private RouteDAO routeDao;
	private RouteEntryDao routeEntryDao;
	
	public AdminOperationsImpl()
	{
		clientDao=new ClientDao();
		cityDao=new CityDAO();
		packageDao=new PackageDAO();
		routeDao=new RouteDAO();
		routeEntryDao=new RouteEntryDao();
	}
	
	@WebMethod
	public String sayHello()
	{
		return "Hello";
	}
	
	
	@WebMethod
    public Client login(String email,String password)
    {
		Client client=clientDao.findClient(email, password);
		return client;
	
    }
	
	@WebMethod
    public Client findClient(String email)
	{
		return clientDao.findClientByEmail(email);
		
	}
	
	@WebMethod
    public int addClient(String name,String email,String password,boolean isAdmin)
	{
		return clientDao.addclient(name, email, password, isAdmin);
		
	}
	
	@WebMethod
    public int addPackage(Package pack)
    {
		City senderCity=pack.getSenderCity();
		City destinationCity=pack.getReceiverCity();
		
		Client sender=pack.getSender();
		Client receiver=pack.getReceiver();
		
		sender.setId((clientDao.addclient(sender.getName(),sender.getEmail(),sender.getEmail(),false)));
		receiver.setId(clientDao.addclient(receiver.getName(),receiver.getEmail(),receiver.getEmail(),false));
		
		System.out.println("senderId "+sender.getId()+" receiverId "+receiver.getId());
		senderCity.setId(cityDao.addCity(senderCity));
		destinationCity.setId(cityDao.addCity(destinationCity));
		
		return packageDao.addPackage(pack);
    }
	
	@WebMethod
    public int registerForTracking(int id,City sourceCity)
    {
		Package pack=packageDao.findPackage(id);
		if(pack!=null)
		{
			if(pack.getIsTracking()==false)
			{
				sourceCity.setId(cityDao.addCity(sourceCity));
				
				//add a new route
				Route route=new Route();
				route.setId(routeDao.addRoute(route));
		
				pack.setRoute(route);
				pack.setIsTracking(true);
				
				packageDao.updatePackage(pack);
				
				//add new route entry
				RouteEntry routeEntry=new RouteEntry();
				routeEntry.setCity(sourceCity);
				routeEntry.setRoute(pack.getRoute());
				routeEntry.setTimestamp(new Timestamp(System.currentTimeMillis()));
				
				return routeEntryDao.addRoute(routeEntry);
				
				
			}
		}
		return -1;
    }
	
	@WebMethod
    public int addNewRoute(int id,City sourceCity)
    {
		packageDao=new PackageDAO();
		routeDao=new RouteDAO();
		cityDao=new CityDAO();
		routeEntryDao=new RouteEntryDao();
		Package pack=packageDao.findPackage(id);
		if(pack!=null)
		{
			if(pack.getIsTracking()==true)
			{
				sourceCity.setId(cityDao.addCity(sourceCity));
				RouteEntry routeEntry=new RouteEntry();
				routeEntry.setCity(sourceCity);
				routeEntry.setRoute(pack.getRoute());
				routeEntry.setTimestamp(new Timestamp(System.currentTimeMillis()));
				
				return routeEntryDao.addRoute(routeEntry);
			
			}
		}
		return -1;
    }
	
	

	@WebMethod
	public List<Package> findAllPackagesByTrackedStatus(boolean isTracked)
    {
		return packageDao.findAllPackagesByTrackStatus(isTracked);
    }
	
	
	
	@WebMethod
    public String showInformtionsAboutPackage(int id)
    {
		Package pack=packageDao.findPackage(id);
		
		String response=pack.getName()+"\n"+pack.getDescription()+"\n"
				+ "sender="+pack.getSender().getEmail()+" \n"
				+ "senderCity="+pack.getSenderCity().getPostalCode()+"\n"
						+ "receiver= "+pack.getReceiver().getEmail()+"\n"
								+ "receiverCity="+pack.getReceiverCity().getPostalCode();
		return response;
    }
	
	@WebMethod
    public int deletePackage(int id)
    {
		return packageDao.deletePackage(id);
    }
	
}
