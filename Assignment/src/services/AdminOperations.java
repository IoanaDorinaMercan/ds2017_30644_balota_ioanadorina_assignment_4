package services;

import java.sql.Timestamp;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import entities.City;
import entities.Client;
import entities.Package;

@WebService
@SOAPBinding(style = Style.DOCUMENT)
public interface AdminOperations {

	@WebMethod
    String sayHello();
	
	@WebMethod
    Client login(String cnp,String password);
	
	@WebMethod
    Client findClient(String email);
	
	@WebMethod
    int addClient(String name,String cnp,String password,boolean isAdmin);
	
	@WebMethod
    int addPackage(Package pack);
	
	@WebMethod
    int registerForTracking(int id,City sourceCity);
	
	@WebMethod
    int addNewRoute(int id,City sourceCity);
	
	@WebMethod
    List<Package> findAllPackagesByTrackedStatus(boolean isTracked);
	
	
	@WebMethod
    String showInformtionsAboutPackage(int id);
	
	@WebMethod
    int deletePackage(int id);
	
}
	
