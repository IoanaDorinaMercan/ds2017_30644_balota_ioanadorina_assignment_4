
package dotNet.serv;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the dotNet.serv package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dotNet.serv
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SayHello }
     * 
     */
    public SayHello createSayHello() {
        return new SayHello();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link GetAllMyPackagesResponse }
     * 
     */
    public GetAllMyPackagesResponse createGetAllMyPackagesResponse() {
        return new GetAllMyPackagesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPackage }
     * 
     */
    public ArrayOfPackage createArrayOfPackage() {
        return new ArrayOfPackage();
    }

    /**
     * Create an instance of {@link SearchPackagesByNameAndDescriptionResponse }
     * 
     */
    public SearchPackagesByNameAndDescriptionResponse createSearchPackagesByNameAndDescriptionResponse() {
        return new SearchPackagesByNameAndDescriptionResponse();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link GetAllMyPackages }
     * 
     */
    public GetAllMyPackages createGetAllMyPackages() {
        return new GetAllMyPackages();
    }

    /**
     * Create an instance of {@link CheckPackageStatus }
     * 
     */
    public CheckPackageStatus createCheckPackageStatus() {
        return new CheckPackageStatus();
    }

    /**
     * Create an instance of {@link SayHelloResponse }
     * 
     */
    public SayHelloResponse createSayHelloResponse() {
        return new SayHelloResponse();
    }

    /**
     * Create an instance of {@link SearchPackagesByNameAndDescription }
     * 
     */
    public SearchPackagesByNameAndDescription createSearchPackagesByNameAndDescription() {
        return new SearchPackagesByNameAndDescription();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link CheckPackageStatusResponse }
     * 
     */
    public CheckPackageStatusResponse createCheckPackageStatusResponse() {
        return new CheckPackageStatusResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRouteEntry }
     * 
     */
    public ArrayOfRouteEntry createArrayOfRouteEntry() {
        return new ArrayOfRouteEntry();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

    /**
     * Create an instance of {@link RouteEntry }
     * 
     */
    public RouteEntry createRouteEntry() {
        return new RouteEntry();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link City }
     * 
     */
    public City createCity() {
        return new City();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

}
