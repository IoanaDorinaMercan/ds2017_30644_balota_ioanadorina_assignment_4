
package dotNet.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="searchPackagesByNameAndDescriptionResult" type="{http://tempuri.org/}ArrayOfPackage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchPackagesByNameAndDescriptionResult"
})
@XmlRootElement(name = "searchPackagesByNameAndDescriptionResponse")
public class SearchPackagesByNameAndDescriptionResponse {

    protected ArrayOfPackage searchPackagesByNameAndDescriptionResult;

    /**
     * Gets the value of the searchPackagesByNameAndDescriptionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPackage }
     *     
     */
    public ArrayOfPackage getSearchPackagesByNameAndDescriptionResult() {
        return searchPackagesByNameAndDescriptionResult;
    }

    /**
     * Sets the value of the searchPackagesByNameAndDescriptionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPackage }
     *     
     */
    public void setSearchPackagesByNameAndDescriptionResult(ArrayOfPackage value) {
        this.searchPackagesByNameAndDescriptionResult = value;
    }

}
