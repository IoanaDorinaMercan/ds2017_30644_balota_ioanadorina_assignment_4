
package dotNet.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="checkPackageStatusResult" type="{http://tempuri.org/}ArrayOfRouteEntry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkPackageStatusResult"
})
@XmlRootElement(name = "checkPackageStatusResponse")
public class CheckPackageStatusResponse {

    protected ArrayOfRouteEntry checkPackageStatusResult;

    /**
     * Gets the value of the checkPackageStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRouteEntry }
     *     
     */
    public ArrayOfRouteEntry getCheckPackageStatusResult() {
        return checkPackageStatusResult;
    }

    /**
     * Sets the value of the checkPackageStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRouteEntry }
     *     
     */
    public void setCheckPackageStatusResult(ArrayOfRouteEntry value) {
        this.checkPackageStatusResult = value;
    }

}
