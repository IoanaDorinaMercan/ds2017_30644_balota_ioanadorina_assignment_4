
package dotNet.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getAllMyPackagesResult" type="{http://tempuri.org/}ArrayOfPackage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllMyPackagesResult"
})
@XmlRootElement(name = "getAllMyPackagesResponse")
public class GetAllMyPackagesResponse {

    protected ArrayOfPackage getAllMyPackagesResult;

    /**
     * Gets the value of the getAllMyPackagesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPackage }
     *     
     */
    public ArrayOfPackage getGetAllMyPackagesResult() {
        return getAllMyPackagesResult;
    }

    /**
     * Sets the value of the getAllMyPackagesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPackage }
     *     
     */
    public void setGetAllMyPackagesResult(ArrayOfPackage value) {
        this.getAllMyPackagesResult = value;
    }

}
