package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import entities.Client;
import hibernate.HibernateUtil;

public class ClientDao {

	public int addclient(String name,String email,String password, boolean isAdmin) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		int exists=verifyIfClientExists(email);
		if(exists==-1) {
		Client client=new Client();
		client.setName(name);
		client.setPassword(password);
		client.setIsAdmin(isAdmin);
		client.setEmail(email);

		int clientId = -1;

		try {
			transaction = session.beginTransaction();
			clientId = (Integer) session.save(client);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return clientId;
		}
		else return exists;
	}

	public Client findClient(String email, String password) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Client> clients = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Client WHERE email=:email AND password=:password");
			query.setParameter("email", email);
			query.setParameter("password", password);
			clients = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error find client DAO");
		} finally {
			session.close();
		}
		if (clients.size() > 0)
			return clients.get(0);
		else
			return null;
	}
	
	
	public Client findClientByEmail(String email) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Client> clients = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Client WHERE email=:email");
			query.setParameter("email", email);
			clients = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error find client DAO");
		} finally {
			session.close();
		}
		if (clients.size() > 0)
			return clients.get(0);
		else
			return null;
	}
	
	private int verifyIfClientExists(String email)
	{
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Client> clients = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Client WHERE email=:email");
			query.setParameter("email", email);
			clients = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error find client DAO");
		} finally {
			session.close();
		}
		if (clients.size() > 0)
			return clients.get(0).getId();
		else
			return -1;
	}
	
	
}
