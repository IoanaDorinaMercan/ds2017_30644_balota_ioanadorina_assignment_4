package dao;

import java.util.List;
import entities.Package;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import hibernate.HibernateUtil;

public class PackageDAO {

	public int addPackage(Package pack)
	{
			Session session = HibernateUtil.openSession();
			Transaction transaction = null;

			int packageId = -1;

			try {
				transaction = session.beginTransaction();
				packageId = (Integer) session.save(pack);
				transaction.commit();
			} catch (HibernateException e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			return packageId;
	}
	
	public void updatePackage(Package pack)
	{
			Session session = HibernateUtil.openSession();
			Transaction transaction = null;
			try {
				transaction = session.beginTransaction();
				session.saveOrUpdate(pack);
				transaction.commit();
			} catch (HibernateException e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
	}
	
	
	public int deletePackage(int id)
	{
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Package>packages=null;
		int result=-1;
		try {
		transaction = session.beginTransaction();
		Query query = session.createQuery("FROM Package WHERE id=:id");
		query.setParameter("id", id);
		packages= query.list();
	
		if (packages != null && !packages.isEmpty()) {
			session.delete(packages.get(0));
			result = (packages.get(0)).getId();
			}
		transaction.commit();
		}
		catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error delete package DAO");
		} finally {
			session.close();
		}
		return result;
	}
	
	public Package findPackage(int id)
	{
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Package>packages=null;
		int result=-1;
		try {
		transaction = session.beginTransaction();
		Query query = session.createQuery("FROM Package WHERE id=:id");
		query.setParameter("id", id);
		packages= query.list();
		transaction.commit();
		}
		catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Errorfind package DAO");
		} finally {
			session.close();
		}
		if(packages.isEmpty()||packages.size()==0)
		return null;
		else return packages.get(0);
	}
	
	
	
	public List<Package> findAllPackagesByTrackStatus(boolean isTracked)
	{
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Package>packages=null;
		int result=-1;
		try {
		transaction = session.beginTransaction();
		Query query = session.createQuery("FROM Package WHERE isTracking=:tracking");
		query.setParameter("tracking", isTracked);
		packages= query.list();
		transaction.commit();
		}
		catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Errorfind package DAO");
		} finally {
			session.close();
		}
		return packages;
	}
	public int updateStatus(int id)
	{
		return 0;
	}
}
