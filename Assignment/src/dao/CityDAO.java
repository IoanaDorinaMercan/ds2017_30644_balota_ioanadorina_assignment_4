package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.City;
import hibernate.HibernateUtil;

public class CityDAO {

	public int addCity(City city) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		int cityId = -1;

		if (verifyIfCityExists(city) == -1) {
			try {
				transaction = session.beginTransaction();
				cityId = (Integer) session.save(city);
				transaction.commit();
			} catch (HibernateException e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			return cityId;
		} else
			return verifyIfCityExists(city);
	}

	public List<City> getAllCities() {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<City> cities = null;
		try {
			transaction = session.beginTransaction();
			cities = session.createQuery("FROM City").list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error get all cities DAO");
		} finally {
			session.close();
		}
		return cities;

	}

	public City getCity(int id) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<City> cityFromDatabase = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE cityId=:cityId");
			query.setParameter("cityId", id);
			cityFromDatabase = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error get an specific city DAO");
		} finally {
			session.close();
		}
		if (cityFromDatabase == null || cityFromDatabase.isEmpty())
			return null;
		else
			return cityFromDatabase.get(0);

	}

	private int verifyIfCityExists(City city) {

		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<City> cityFromDatabase = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE postalCode=:postalCode");
			query.setParameter("postalCode", city.getPostalCode());
			cityFromDatabase = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error verify if city exists DAO");
		} finally {
			session.close();
		}
		if (cityFromDatabase == null || cityFromDatabase.isEmpty())
			return -1;
		else
			return cityFromDatabase.get(0).getId();
	}
}
