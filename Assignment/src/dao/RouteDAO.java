package dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import entities.City;
import entities.Route;
import hibernate.HibernateUtil;

public class RouteDAO {

	public int addRoute(Route route) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		int routeId = -1;


			try {
				transaction = session.beginTransaction();
				routeId = (Integer)session.save(route);
				transaction.commit();
			} catch (HibernateException e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			return routeId;
		} 
	
	public Route getEntry(int id) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		int routeId = -1;
		List<Route>routes=null;

			try {
				transaction = session.beginTransaction();
				Query query = session.createQuery("FROM Route WHERE id=:id");
				query.setParameter("id", id);
				routes= query.list();
				transaction.commit();	
			} catch (HibernateException e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			if(routes==null||routes.isEmpty())return null;
			else return routes.get(0);
		} 
	
}
