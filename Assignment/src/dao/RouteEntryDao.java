package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.Route;
import entities.RouteEntry;
import hibernate.HibernateUtil;

public class RouteEntryDao {

	public int addRoute(RouteEntry routeEntry) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		int routeId = -1;


			try {
				transaction = session.beginTransaction();
				routeId = (Integer)session.save(routeEntry);
				transaction.commit();
			} catch (HibernateException e) {
				if (transaction != null)
					transaction.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			return routeId;
		} 
}
