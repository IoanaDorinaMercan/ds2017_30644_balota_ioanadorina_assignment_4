/**
 * AdminOperationsImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface AdminOperationsImpl extends java.rmi.Remote {
    public entities.Client login(java.lang.String cnp, java.lang.String password) throws java.rmi.RemoteException;
    public int addPackage(entities._package pack) throws java.rmi.RemoteException;
    public java.lang.String sayHello() throws java.rmi.RemoteException;
    public entities.Client addClient(java.lang.String cnp, java.lang.String password) throws java.rmi.RemoteException;
}
