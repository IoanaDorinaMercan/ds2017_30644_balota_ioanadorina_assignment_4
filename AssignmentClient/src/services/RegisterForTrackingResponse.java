/**
 * RegisterForTrackingResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class RegisterForTrackingResponse  implements java.io.Serializable {
    private int registerForTrackingReturn;

    public RegisterForTrackingResponse() {
    }

    public RegisterForTrackingResponse(
           int registerForTrackingReturn) {
           this.registerForTrackingReturn = registerForTrackingReturn;
    }


    /**
     * Gets the registerForTrackingReturn value for this RegisterForTrackingResponse.
     * 
     * @return registerForTrackingReturn
     */
    public int getRegisterForTrackingReturn() {
        return registerForTrackingReturn;
    }


    /**
     * Sets the registerForTrackingReturn value for this RegisterForTrackingResponse.
     * 
     * @param registerForTrackingReturn
     */
    public void setRegisterForTrackingReturn(int registerForTrackingReturn) {
        this.registerForTrackingReturn = registerForTrackingReturn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegisterForTrackingResponse)) return false;
        RegisterForTrackingResponse other = (RegisterForTrackingResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.registerForTrackingReturn == other.getRegisterForTrackingReturn();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getRegisterForTrackingReturn();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegisterForTrackingResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services", ">registerForTrackingResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerForTrackingReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services", "registerForTrackingReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
