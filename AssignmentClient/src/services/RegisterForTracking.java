/**
 * RegisterForTracking.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class RegisterForTracking  implements java.io.Serializable {
    private int id;

    private entities.City sourceCity;

    private java.lang.Object timestamp;

    public RegisterForTracking() {
    }

    public RegisterForTracking(
           int id,
           entities.City sourceCity,
           java.lang.Object timestamp) {
           this.id = id;
           this.sourceCity = sourceCity;
           this.timestamp = timestamp;
    }


    /**
     * Gets the id value for this RegisterForTracking.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this RegisterForTracking.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the sourceCity value for this RegisterForTracking.
     * 
     * @return sourceCity
     */
    public entities.City getSourceCity() {
        return sourceCity;
    }


    /**
     * Sets the sourceCity value for this RegisterForTracking.
     * 
     * @param sourceCity
     */
    public void setSourceCity(entities.City sourceCity) {
        this.sourceCity = sourceCity;
    }


    /**
     * Gets the timestamp value for this RegisterForTracking.
     * 
     * @return timestamp
     */
    public java.lang.Object getTimestamp() {
        return timestamp;
    }


    /**
     * Sets the timestamp value for this RegisterForTracking.
     * 
     * @param timestamp
     */
    public void setTimestamp(java.lang.Object timestamp) {
        this.timestamp = timestamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegisterForTracking)) return false;
        RegisterForTracking other = (RegisterForTracking) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.id == other.getId() &&
            ((this.sourceCity==null && other.getSourceCity()==null) || 
             (this.sourceCity!=null &&
              this.sourceCity.equals(other.getSourceCity()))) &&
            ((this.timestamp==null && other.getTimestamp()==null) || 
             (this.timestamp!=null &&
              this.timestamp.equals(other.getTimestamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getId();
        if (getSourceCity() != null) {
            _hashCode += getSourceCity().hashCode();
        }
        if (getTimestamp() != null) {
            _hashCode += getTimestamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegisterForTracking.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services", ">registerForTracking"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services", "sourceCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "City"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services", "timestamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
