/**
 * AdminOperationsImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class AdminOperationsImplServiceLocator extends org.apache.axis.client.Service implements services.AdminOperationsImplService {

    public AdminOperationsImplServiceLocator() {
    }


    public AdminOperationsImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AdminOperationsImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for AdminOperationsImpl
    private java.lang.String AdminOperationsImpl_address = "http://localhost:8080/Assignment/services/AdminOperationsImpl";

    public java.lang.String getAdminOperationsImplAddress() {
        return AdminOperationsImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String AdminOperationsImplWSDDServiceName = "AdminOperationsImpl";

    public java.lang.String getAdminOperationsImplWSDDServiceName() {
        return AdminOperationsImplWSDDServiceName;
    }

    public void setAdminOperationsImplWSDDServiceName(java.lang.String name) {
        AdminOperationsImplWSDDServiceName = name;
    }

    public services.AdminOperationsImpl getAdminOperationsImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(AdminOperationsImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getAdminOperationsImpl(endpoint);
    }

    public services.AdminOperationsImpl getAdminOperationsImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            services.AdminOperationsImplSoapBindingStub _stub = new services.AdminOperationsImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getAdminOperationsImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setAdminOperationsImplEndpointAddress(java.lang.String address) {
        AdminOperationsImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (services.AdminOperationsImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                services.AdminOperationsImplSoapBindingStub _stub = new services.AdminOperationsImplSoapBindingStub(new java.net.URL(AdminOperationsImpl_address), this);
                _stub.setPortName(getAdminOperationsImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("AdminOperationsImpl".equals(inputPortName)) {
            return getAdminOperationsImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services", "AdminOperationsImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services", "AdminOperationsImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("AdminOperationsImpl".equals(portName)) {
            setAdminOperationsImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
