/**
 * AddClientResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class AddClientResponse  implements java.io.Serializable {
    private entities.Client addClientReturn;

    public AddClientResponse() {
    }

    public AddClientResponse(
           entities.Client addClientReturn) {
           this.addClientReturn = addClientReturn;
    }


    /**
     * Gets the addClientReturn value for this AddClientResponse.
     * 
     * @return addClientReturn
     */
    public entities.Client getAddClientReturn() {
        return addClientReturn;
    }


    /**
     * Sets the addClientReturn value for this AddClientResponse.
     * 
     * @param addClientReturn
     */
    public void setAddClientReturn(entities.Client addClientReturn) {
        this.addClientReturn = addClientReturn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddClientResponse)) return false;
        AddClientResponse other = (AddClientResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addClientReturn==null && other.getAddClientReturn()==null) || 
             (this.addClientReturn!=null &&
              this.addClientReturn.equals(other.getAddClientReturn())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddClientReturn() != null) {
            _hashCode += getAddClientReturn().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddClientResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services", ">addClientResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addClientReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services", "addClientReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "Client"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
