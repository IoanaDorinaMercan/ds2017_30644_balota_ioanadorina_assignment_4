package services;

public class AdminOperationsImplProxy implements services.AdminOperationsImpl {
  private String _endpoint = null;
  private services.AdminOperationsImpl adminOperationsImpl = null;
  
  public AdminOperationsImplProxy() {
    _initAdminOperationsImplProxy();
  }
  
  public AdminOperationsImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initAdminOperationsImplProxy();
  }
  
  private void _initAdminOperationsImplProxy() {
    try {
      adminOperationsImpl = (new services.AdminOperationsImplServiceLocator()).getAdminOperationsImpl();
      if (adminOperationsImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)adminOperationsImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)adminOperationsImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (adminOperationsImpl != null)
      ((javax.xml.rpc.Stub)adminOperationsImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public services.AdminOperationsImpl getAdminOperationsImpl() {
    if (adminOperationsImpl == null)
      _initAdminOperationsImplProxy();
    return adminOperationsImpl;
  }
  
  public entities.Client login(java.lang.String cnp, java.lang.String password) throws java.rmi.RemoteException{
    if (adminOperationsImpl == null)
      _initAdminOperationsImplProxy();
    return adminOperationsImpl.login(cnp, password);
  }
  
  public int addPackage(entities._package pack) throws java.rmi.RemoteException{
    if (adminOperationsImpl == null)
      _initAdminOperationsImplProxy();
    return adminOperationsImpl.addPackage(pack);
  }
  
  public java.lang.String sayHello() throws java.rmi.RemoteException{
    if (adminOperationsImpl == null)
      _initAdminOperationsImplProxy();
    return adminOperationsImpl.sayHello();
  }
  
  public entities.Client addClient(java.lang.String cnp, java.lang.String password) throws java.rmi.RemoteException{
    if (adminOperationsImpl == null)
      _initAdminOperationsImplProxy();
    return adminOperationsImpl.addClient(cnp, password);
  }
  
  
}