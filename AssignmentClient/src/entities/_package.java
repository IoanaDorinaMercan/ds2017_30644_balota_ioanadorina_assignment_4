/**
 * _package.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package entities;

public class _package  implements java.io.Serializable {
    private java.lang.String description;

    private entities.City destinationCity;

    private int id;

    private java.lang.String name;

    private entities.Client receiver;

    private entities.Route route;

    private entities.Client sender;

    private entities.City senderCity;

    private boolean tracking;

    public _package() {
    }

    public _package(
           java.lang.String description,
           entities.City destinationCity,
           int id,
           java.lang.String name,
           entities.Client receiver,
           entities.Route route,
           entities.Client sender,
           entities.City senderCity,
           boolean tracking) {
           this.description = description;
           this.destinationCity = destinationCity;
           this.id = id;
           this.name = name;
           this.receiver = receiver;
           this.route = route;
           this.sender = sender;
           this.senderCity = senderCity;
           this.tracking = tracking;
    }


    /**
     * Gets the description value for this _package.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this _package.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the destinationCity value for this _package.
     * 
     * @return destinationCity
     */
    public entities.City getDestinationCity() {
        return destinationCity;
    }


    /**
     * Sets the destinationCity value for this _package.
     * 
     * @param destinationCity
     */
    public void setDestinationCity(entities.City destinationCity) {
        this.destinationCity = destinationCity;
    }


    /**
     * Gets the id value for this _package.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this _package.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the name value for this _package.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this _package.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the receiver value for this _package.
     * 
     * @return receiver
     */
    public entities.Client getReceiver() {
        return receiver;
    }


    /**
     * Sets the receiver value for this _package.
     * 
     * @param receiver
     */
    public void setReceiver(entities.Client receiver) {
        this.receiver = receiver;
    }


    /**
     * Gets the route value for this _package.
     * 
     * @return route
     */
    public entities.Route getRoute() {
        return route;
    }


    /**
     * Sets the route value for this _package.
     * 
     * @param route
     */
    public void setRoute(entities.Route route) {
        this.route = route;
    }


    /**
     * Gets the sender value for this _package.
     * 
     * @return sender
     */
    public entities.Client getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this _package.
     * 
     * @param sender
     */
    public void setSender(entities.Client sender) {
        this.sender = sender;
    }


    /**
     * Gets the senderCity value for this _package.
     * 
     * @return senderCity
     */
    public entities.City getSenderCity() {
        return senderCity;
    }


    /**
     * Sets the senderCity value for this _package.
     * 
     * @param senderCity
     */
    public void setSenderCity(entities.City senderCity) {
        this.senderCity = senderCity;
    }


    /**
     * Gets the tracking value for this _package.
     * 
     * @return tracking
     */
    public boolean isTracking() {
        return tracking;
    }


    /**
     * Sets the tracking value for this _package.
     * 
     * @param tracking
     */
    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof _package)) return false;
        _package other = (_package) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.destinationCity==null && other.getDestinationCity()==null) || 
             (this.destinationCity!=null &&
              this.destinationCity.equals(other.getDestinationCity()))) &&
            this.id == other.getId() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.receiver==null && other.getReceiver()==null) || 
             (this.receiver!=null &&
              this.receiver.equals(other.getReceiver()))) &&
            ((this.route==null && other.getRoute()==null) || 
             (this.route!=null &&
              this.route.equals(other.getRoute()))) &&
            ((this.sender==null && other.getSender()==null) || 
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.senderCity==null && other.getSenderCity()==null) || 
             (this.senderCity!=null &&
              this.senderCity.equals(other.getSenderCity()))) &&
            this.tracking == other.isTracking();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getDestinationCity() != null) {
            _hashCode += getDestinationCity().hashCode();
        }
        _hashCode += getId();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getReceiver() != null) {
            _hashCode += getReceiver().hashCode();
        }
        if (getRoute() != null) {
            _hashCode += getRoute().hashCode();
        }
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getSenderCity() != null) {
            _hashCode += getSenderCity().hashCode();
        }
        _hashCode += (isTracking() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(_package.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities", "Package"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "destinationCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "City"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "receiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "Client"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("route");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "route"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "Route"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "Client"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("senderCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "senderCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities", "City"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tracking");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities", "tracking"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
