drop database assignment4;
create database assignment4;
use assignment4;

create table client(
id int primary key unique auto_increment,
email varchar(100),
isAdmin boolean,
name varchar(50),
password varchar(50)
)

create table city
(id int primary key unique auto_increment,
name varchar(100),
postalCode varchar(100) unique)

create table route
(id int primary key unique auto_increment,
entry varchar(500))

drop table package;
create table package
(id int primary key unique auto_increment,
sender_id int,
receiver_id int,
name varchar(100),
description varchar(500),
senderCity_id int,
receiverCit_id int,
tracking boolean,
routeId int,
FOREIGN KEY (sender_id) references client(id),
FOREIGN KEY (receiver_id) references client(id),
FOREIGN KEY (senderCity_id) references city(id),
FOREIGN KEY (receiverCity_id) references city(id),
FOREIGN KEY (route_id) references route(id))


