﻿using System;
using System.Collections.Generic;
using System.Web;

using MySql.Data;
using MySql.Data.MySqlClient;


/// <summary>
/// Summary description for MySQL
/// </summary>
public class ClientDao
{
    MySql.Data.MySqlClient.MySqlConnection conn;
    string myConnectionString = "server=127.0.0.1;uid=root;" +
            "pwd=root;database=assignment4";
    public ClientDao()
    { 


    }

    public void connect()
    {
        try
        {
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
        }
        catch (MySql.Data.MySqlClient.MySqlException ex)
        {
            Console.Write("eroare conectare");
        }
    }

    public void disconnect()
    {
        conn.Close();
    }
    public Client login(string email,string password)
    {
        connect();

        Client client = new Client();
        string email1=null;
        try
        {
            string sql = "SELECT * FROM client WHERE email='" + email + "'" + "AND password='" + password + "'";
          //  string sql = "SELECT * FROM client WHERE email='" + "dorina@yahoo.com" + "'" + "AND password='" + "123456" + "'";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
           
            while (rdr.Read())
            {
                // client.setEmail(rdr.GetString(rdr.GetOrdinal("email")));
                email1 = rdr.GetString(rdr.GetOrdinal("email"));
                client.setEmail(rdr.GetString(rdr.GetOrdinal("email")));
                client.setName(rdr.GetString(rdr.GetOrdinal("name")));
                client.setIsAdmin(rdr.GetBoolean(rdr.GetOrdinal("isAdmin")));
                client.setPassword(rdr.GetString(rdr.GetOrdinal("password")));
                client.setId(rdr.GetInt32(rdr.GetOrdinal("id")));
            }
            rdr.Close();
        }

        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        disconnect();
        return client;
    }


    public int register(string nume, string email, string password)
    {
        connect();
        MySqlCommand cmd = new MySqlCommand("INSERT INTO client(email, isAdmin, name, password) values (@email,@isAdmin,@name,@password) ", conn);
        cmd.Parameters.AddWithValue("@email", email);
        cmd.Parameters.AddWithValue("@isAdmin", false);
        cmd.Parameters.AddWithValue("@name", nume);
        cmd.Parameters.AddWithValue("@password", password);
        int id = -1;
        if(cmd.ExecuteNonQuery()>0)
        {
            MySqlCommand cmdSelect = new MySqlCommand("SELECT * FROM client WHERE email=@email", conn);
            cmdSelect.Parameters.AddWithValue("@email", email);
            MySqlDataReader rdr = cmdSelect.ExecuteReader();
            while (rdr.Read())
            {
                id=(rdr.GetInt32(rdr.GetOrdinal("id")));
            }
            rdr.Close();
        }
        disconnect();
        return id;
    }

    public List<Package> getAllMyPackages(int clientId)
    {
        connect();
        MySqlCommand cmdSelect = new MySqlCommand("SELECT * FROM package WHERE sender_id=@sender_id", conn);
        cmdSelect.Parameters.AddWithValue("@sender_id", clientId);
        MySqlDataReader rdr = cmdSelect.ExecuteReader();

        List<Package> packages = new List<Package>();

        while (rdr.Read())
        {
            Package pack = new Package();
            pack.setId(rdr.GetInt32(rdr.GetOrdinal("id")));

            //create a new connection
            MySql.Data.MySqlClient.MySqlConnection conn2=null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }

            MySqlCommand cmdSelectSender = new MySqlCommand("SELECT * FROM client WHERE id=@id", conn2);
            cmdSelectSender.Parameters.AddWithValue("@id", clientId);
            MySqlDataReader rdrSelectSender = cmdSelectSender.ExecuteReader();

            //sender
            Client sender = new Client();
            while(rdrSelectSender.Read())
            {
                sender.setId(rdrSelectSender.GetInt32(rdrSelectSender.GetOrdinal("id")));
                sender.setName(rdrSelectSender.GetString(rdrSelectSender.GetOrdinal("name")));
                sender.setEmail(rdrSelectSender.GetString(rdrSelectSender.GetOrdinal("email")));
                sender.setPassword(rdrSelectSender.GetString(rdrSelectSender.GetOrdinal("password")));
                sender.setIsAdmin(rdrSelectSender.GetBoolean(rdrSelectSender.GetOrdinal("isAdmin")));
            }
            pack.setSender(sender);
            rdrSelectSender.Close();
            conn2.Close();

            //receiver
            //create a new connection
            conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }
            int receiverId = rdr.GetInt32(rdr.GetOrdinal("receiver_id"));
            MySqlCommand cmdSelectReceiver = new MySqlCommand("SELECT * FROM client WHERE id=@id", conn2);
            cmdSelectReceiver.Parameters.AddWithValue("@id", receiverId);
            MySqlDataReader rdrSelectReceiver = cmdSelectReceiver.ExecuteReader();

            Client receiver = new Client();
            while (rdrSelectReceiver.Read())
            {
                receiver.setId(rdrSelectReceiver.GetInt32(rdrSelectReceiver.GetOrdinal("id")));
                receiver.setName(rdrSelectReceiver.GetString(rdrSelectReceiver.GetOrdinal("name")));
                receiver.setEmail(rdrSelectReceiver.GetString(rdrSelectReceiver.GetOrdinal("email")));
                receiver.setPassword(rdrSelectReceiver.GetString(rdrSelectReceiver.GetOrdinal("password")));
                receiver.setIsAdmin(rdrSelectReceiver.GetBoolean(rdrSelectReceiver.GetOrdinal("isAdmin")));
            }
            pack.setReceiver(receiver);
            rdrSelectReceiver.Close();
            conn2.Close();

            //sender city id
            //create a new connection
            conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }
            int senderCityId= rdr.GetInt32(rdr.GetOrdinal("senderCity_id"));
            MySqlCommand cmdSelectSenderCity = new MySqlCommand("SELECT * FROM city WHERE id=@id", conn2);
            cmdSelectSenderCity.Parameters.AddWithValue("@id", senderCityId);
            MySqlDataReader rdrSelectSenderCity = cmdSelectSenderCity.ExecuteReader();

            City senderCity = new City();
            while (rdrSelectSenderCity.Read())
            {
                senderCity.setId(rdrSelectSenderCity.GetInt32(rdrSelectSenderCity.GetOrdinal("id")));
                senderCity.setName(rdrSelectSenderCity.GetString(rdrSelectSenderCity.GetOrdinal("name")));
                senderCity.setPostalCode(rdrSelectSenderCity.GetString(rdrSelectSenderCity.GetOrdinal("postalCode")));
            }
            pack.setSenderCity(senderCity);
            rdrSelectSenderCity.Close();
            conn2.Close();

            //receiver city id
            //create a new connection
            conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }
            int receiverCityId = rdr.GetInt32(rdr.GetOrdinal("receiverCity_id"));
            MySqlCommand cmdSelectReceiverCity = new MySqlCommand("SELECT * FROM city WHERE id=@id", conn2);
            cmdSelectReceiverCity.Parameters.AddWithValue("@id", receiverCityId);
            MySqlDataReader rdrSelectReceiverCity = cmdSelectReceiverCity.ExecuteReader();

            City receiverCity = new City();
            while (rdrSelectReceiverCity.Read())
            {
                receiverCity.setId(rdrSelectReceiverCity.GetInt32(rdrSelectReceiverCity.GetOrdinal("id")));
                receiverCity.setName(rdrSelectReceiverCity.GetString(rdrSelectReceiverCity.GetOrdinal("name")));
                receiverCity.setPostalCode(rdrSelectReceiverCity.GetString(rdrSelectReceiverCity.GetOrdinal("postalCode")));
            }
            pack.setReceiverCity(receiverCity);
            rdrSelectReceiverCity.Close();
            conn2.Close();

            pack.setDescription(rdr.GetString(rdr.GetOrdinal("description")));
            pack.setName(rdr.GetString(rdr.GetOrdinal("name")));

            int routeId= (rdr.GetInt32(rdr.GetOrdinal("route_id")));
            Route route = new Route();
            route.setId(routeId);
            pack.setRoute(route);

            packages.Add(pack);

        }
        rdr.Close();
        disconnect();
        return packages;
    }

    public List<RouteEntry> checkPackageStatus(int packageId)
    {
        connect();

        //get route for this package
        MySqlCommand cmdSelect = new MySqlCommand("SELECT * FROM package WHERE id=@id", conn);
        cmdSelect.Parameters.AddWithValue("@id", packageId);
        MySqlDataReader rdr = cmdSelect.ExecuteReader();
        int routeId = 0;
        while(rdr.Read())
        {
            routeId = (rdr.GetInt32(rdr.GetOrdinal("route_id")));
        }
        rdr.Close();
        disconnect();

        //get all route entries for this route
        MySql.Data.MySqlClient.MySqlConnection conn1 = null;
        try
        {
            conn1 = new MySql.Data.MySqlClient.MySqlConnection();
            conn1.ConnectionString = myConnectionString;
            conn1.Open();
        }
        catch (MySql.Data.MySqlClient.MySqlException ex)
        {
            Console.Write("eroare conectare");
        }
        List<RouteEntry> entries = new List<RouteEntry>();
        MySqlCommand cmdSelect1 = new MySqlCommand("SELECT * FROM routeentry WHERE route_id=@id", conn1);
        cmdSelect1.Parameters.AddWithValue("@id", routeId);
        MySqlDataReader rdr1 = cmdSelect1.ExecuteReader();
        while (rdr1.Read())
        {
            RouteEntry entry = new RouteEntry();
            entry.setId(rdr1.GetInt32(rdr1.GetOrdinal("id")));
            entry.setTimestamp(rdr1.GetDateTime(rdr1.GetOrdinal("timestamp")));
            int cityId= rdr1.GetInt32(rdr1.GetOrdinal("city_id"));

            MySql.Data.MySqlClient.MySqlConnection conn2 = null; 
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }

            MySqlCommand cmdSelectCity = new MySqlCommand("SELECT * FROM city WHERE id=@id", conn2);
            cmdSelectCity.Parameters.AddWithValue("@id", cityId);
            MySqlDataReader rdrCity = cmdSelectCity.ExecuteReader();
            City city = new City();
            while(rdrCity.Read())
            {
                city.setId(cityId);
                city.setName(rdrCity.GetString(rdrCity.GetOrdinal("name")));
                city.setPostalCode(rdrCity.GetString(rdrCity.GetOrdinal("postalCode")));
            }
            rdrCity.Close();
            conn2.Close();
            entry.setCity(city);
            entries.Add(entry);
        }
        rdr1.Close();
        conn1.Close();


        return entries;
    }

    public List<Package> searchPackagesByNameAndDescription(int clientId,string packageName,string description)
    {
        connect();
        MySqlCommand cmdSelect = new MySqlCommand("SELECT * FROM package WHERE sender_id=@sender_id AND ( INSTR(name, @name)>0 OR INSTR(description, @description)>0)", conn);
        cmdSelect.Parameters.AddWithValue("@sender_id", clientId);
        cmdSelect.Parameters.AddWithValue("@name", packageName);
        cmdSelect.Parameters.AddWithValue("@description", description);
        MySqlDataReader rdr = cmdSelect.ExecuteReader();

        List<Package> packages = new List<Package>();

        while (rdr.Read())
        {
            Package pack = new Package();
            pack.setId(rdr.GetInt32(rdr.GetOrdinal("id")));

            //create a new connection
            MySql.Data.MySqlClient.MySqlConnection conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }

            MySqlCommand cmdSelectSender = new MySqlCommand("SELECT * FROM client WHERE id=@id", conn2);
            cmdSelectSender.Parameters.AddWithValue("@id", clientId);
            MySqlDataReader rdrSelectSender = cmdSelectSender.ExecuteReader();

            //sender
            Client sender = new Client();
            while (rdrSelectSender.Read())
            {
                sender.setId(rdrSelectSender.GetInt32(rdrSelectSender.GetOrdinal("id")));
                sender.setName(rdrSelectSender.GetString(rdrSelectSender.GetOrdinal("name")));
                sender.setEmail(rdrSelectSender.GetString(rdrSelectSender.GetOrdinal("email")));
                sender.setPassword(rdrSelectSender.GetString(rdrSelectSender.GetOrdinal("password")));
                sender.setIsAdmin(rdrSelectSender.GetBoolean(rdrSelectSender.GetOrdinal("isAdmin")));
            }
            pack.setSender(sender);
            rdrSelectSender.Close();
            conn2.Close();

            //receiver
            //create a new connection
            conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }
            int receiverId = rdr.GetInt32(rdr.GetOrdinal("receiver_id"));
            MySqlCommand cmdSelectReceiver = new MySqlCommand("SELECT * FROM client WHERE id=@id", conn2);
            cmdSelectReceiver.Parameters.AddWithValue("@id", receiverId);
            MySqlDataReader rdrSelectReceiver = cmdSelectReceiver.ExecuteReader();

            Client receiver = new Client();
            while (rdrSelectReceiver.Read())
            {
                receiver.setId(rdrSelectReceiver.GetInt32(rdrSelectReceiver.GetOrdinal("id")));
                receiver.setName(rdrSelectReceiver.GetString(rdrSelectReceiver.GetOrdinal("name")));
                receiver.setEmail(rdrSelectReceiver.GetString(rdrSelectReceiver.GetOrdinal("email")));
                receiver.setPassword(rdrSelectReceiver.GetString(rdrSelectReceiver.GetOrdinal("password")));
                receiver.setIsAdmin(rdrSelectReceiver.GetBoolean(rdrSelectReceiver.GetOrdinal("isAdmin")));
            }
            pack.setReceiver(receiver);
            rdrSelectReceiver.Close();
            conn2.Close();

            //sender city id
            //create a new connection
            conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }
            int senderCityId = rdr.GetInt32(rdr.GetOrdinal("senderCity_id"));
            MySqlCommand cmdSelectSenderCity = new MySqlCommand("SELECT * FROM city WHERE id=@id", conn2);
            cmdSelectSenderCity.Parameters.AddWithValue("@id", senderCityId);
            MySqlDataReader rdrSelectSenderCity = cmdSelectSenderCity.ExecuteReader();

            City senderCity = new City();
            while (rdrSelectSenderCity.Read())
            {
                senderCity.setId(rdrSelectSenderCity.GetInt32(rdrSelectSenderCity.GetOrdinal("id")));
                senderCity.setName(rdrSelectSenderCity.GetString(rdrSelectSenderCity.GetOrdinal("name")));
                senderCity.setPostalCode(rdrSelectSenderCity.GetString(rdrSelectSenderCity.GetOrdinal("postalCode")));
            }
            pack.setSenderCity(senderCity);
            rdrSelectSenderCity.Close();
            conn2.Close();

            //receiver city id
            //create a new connection
            conn2 = null;
            try
            {
                conn2 = new MySql.Data.MySqlClient.MySqlConnection();
                conn2.ConnectionString = myConnectionString;
                conn2.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.Write("eroare conectare");
            }
            int receiverCityId = rdr.GetInt32(rdr.GetOrdinal("receiverCity_id"));
            MySqlCommand cmdSelectReceiverCity = new MySqlCommand("SELECT * FROM city WHERE id=@id", conn2);
            cmdSelectReceiverCity.Parameters.AddWithValue("@id", receiverCityId);
            MySqlDataReader rdrSelectReceiverCity = cmdSelectReceiverCity.ExecuteReader();

            City receiverCity = new City();
            while (rdrSelectReceiverCity.Read())
            {
                receiverCity.setId(rdrSelectReceiverCity.GetInt32(rdrSelectReceiverCity.GetOrdinal("id")));
                receiverCity.setName(rdrSelectReceiverCity.GetString(rdrSelectReceiverCity.GetOrdinal("name")));
                receiverCity.setPostalCode(rdrSelectReceiverCity.GetString(rdrSelectReceiverCity.GetOrdinal("postalCode")));
            }
            pack.setReceiverCity(receiverCity);
            rdrSelectReceiverCity.Close();
            conn2.Close();

            pack.setDescription(rdr.GetString(rdr.GetOrdinal("description")));
            pack.setName(rdr.GetString(rdr.GetOrdinal("name")));

            int routeId = (rdr.GetInt32(rdr.GetOrdinal("route_id")));
            Route route = new Route();
            route.setId(routeId);
            pack.setRoute(route);

            packages.Add(pack);

        }
        rdr.Close();
        disconnect();
        return packages;
    }
}