﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for City
/// </summary>
public class City
{

    public int id;
    public String name;
    public String postalCode;

    public City()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getPostalCode()
    {
        return postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

}