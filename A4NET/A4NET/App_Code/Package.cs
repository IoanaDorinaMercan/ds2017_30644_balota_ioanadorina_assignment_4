﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Package
/// </summary>
public class Package
{
    public int id;
    public Client sender;
    public Client receiver;
    public String name;
    public String description;
    public City senderCity;
    public City receiverCity;
    public Boolean tracking;
    public Route route;



    public Package()
    {
    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public Client getSender()
    {
        return sender;
    }
    public void setSender(Client sender)
    {
        this.sender = sender;
    }


    public Client getReceiver()
    {
        return receiver;
    }
    public void setReceiver(Client receiver)
    {
        this.receiver = receiver;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public City getSenderCity()
    {
        return senderCity;
    }
    public void setSenderCity(City senderCity)
    {
        this.senderCity = senderCity;
    }

    public Boolean getIsTracking()
    {
        return tracking;
    }
    public void setIsTracking(Boolean tracking)
    {
        this.tracking = tracking;
    }

    public City getReceiverCity()
    {
        return receiverCity;
    }
    public void setReceiverCity(City receiverCity)
    {
        this.receiverCity = receiverCity;
    }

    public Route getRoute()
    {
        return route;
    }
    public void setRoute(Route route)
    {
        this.route = route;
    }



}