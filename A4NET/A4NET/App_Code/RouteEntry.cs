﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for RouteEntry
/// </summary>
public class RouteEntry
{
    public int id;
    public Route route;
    public City city;
    public DateTime timestamp;

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public Route getRoute()
    {
        return route;
    }
    public void setRoute(Route route)
    {
        this.route = route;
    }

    public City getCity()
    {
        return city;
    }
    public void setCity(City city)
    {
        this.city = city;
    }

    public DateTime getTimestamp()
    {
        return timestamp;
    }
    public void setTimestamp(DateTime timestamp)
    {
        this.timestamp = timestamp;
    }

    public RouteEntry()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}