﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;

/// <summary>
/// Summary description for MyServiceClass
/// </summary>
/// 
[WebService]
public class ClientServiceDotNet
{
    public ClientServiceDotNet()
    {
    }

    [WebMethod]
    public string sayHello()
    {
        return "Hello";
    }

    [WebMethod]
    public Client login(string email,string password)
    {
        ClientDao m = new ClientDao();
        return (m.login(email, password));
    }

    [WebMethod]
    public int register(string nume, string email, string password)
    {
        ClientDao m = new ClientDao();
        return (m.register(nume,email,password));
    }

    [WebMethod]
    public List<Package> getAllMyPackages(int clientId)
    {
        ClientDao m = new ClientDao();
        return (m.getAllMyPackages(clientId));
    }

    [WebMethod]
    public List<RouteEntry> checkPackageStatus(int packageId)
    {
        ClientDao m = new ClientDao();
        return m.checkPackageStatus(packageId);
    }
    [WebMethod]
    public List<Package> searchPackagesByNameAndDescription(int clientId, string packageName,string description)
    {
        ClientDao m = new ClientDao();
        return m.searchPackagesByNameAndDescription(clientId, packageName, description);

    }
}