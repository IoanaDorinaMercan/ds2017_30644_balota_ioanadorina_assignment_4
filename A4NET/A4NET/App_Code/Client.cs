﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Client
/// </summary>
/// 
[Serializable]
public class Client 
{

    public int id;
    public String name;
    public String email;
    public String password;
    public Boolean isAdmin;

    public Client()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }


    public Boolean getIsAdmin()
    {
        return isAdmin;
    }
    public void setIsAdmin(Boolean isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
}